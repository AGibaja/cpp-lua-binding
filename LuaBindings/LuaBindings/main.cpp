
extern "C"{
    #include <lualib.h>
    #include <lauxlib.h>
    #include <lua.h>
}
#include "sol.hpp"


#include <python2.7/Python.h>


static int displayValues()
{
    std::cout << "Displaying values!" << std::endl;
    return 0;
}



int main()
{
    int a = 1;
    std::cout << "Mem address: " << &a << std::endl;
    sol::state state;

    state.open_libraries(sol::lib::base, sol::lib::package);

    state.set_function("displayValues", displayValues);

    //lua_register(state, "displayValues", displayValues);

    luaL_dofile(state, "main.lua");

    lua_getglobal(state, "sayHi");
    lua_pcall(state, 0, 0, 0);

    //state.do_string("displayValues();");

    //lua_close(state);
}
